package view;

import javafx.geometry.Insets;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;

public class RegisterRootPane extends BorderPane {

	//by declaring these as fields, they are accessible throughout the class, 
	//e.g. we can write 'get' methods that return a reference to them - see towards end of class
	private ButtonPane bp;
	private NamePane np;
	private ViewPane vp;
	private RegisterMenuBar rmb;
	
	public RegisterRootPane() {
		this.setStyle("-fx-background-color: #415D78;");
		
		np = new NamePane();
		vp = new ViewPane();
		bp = new ButtonPane();
		rmb = new RegisterMenuBar();
		
		VBox topContainer = new VBox(np, bp);
		topContainer.setSpacing(20); //spacing between np and bp
		topContainer.setPadding(new Insets(0,0,20,0)); //bottom padding between this container and vp
		
		BorderPane rootContainer = new BorderPane();
		rootContainer.setTop(topContainer);
		rootContainer.setCenter(vp);
		rootContainer.setPadding(new Insets(20,20,20,20)); //padding around the entire root container
		
		this.setTop(rmb);
		this.setCenter(rootContainer);
	}
	
	//methods for retrieving the subcontainers of this root pane that can be used by the controller

	/* ---TO DO - implement getNamePane method ---*/
	
	
	/* ---TO DO - implement getButtonPane method ---*/
	
	
	/* ---TO DO - implement getViewPane method ---*/
	
	//allows the menu bar to be retrieved to access its methods and attach listeners
	public RegisterMenuBar getMenuBar() {
		return rmb;
	}
}
