package view;


import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;

public class ButtonPane extends HBox {

	//declared for access throughout class, as handlers are now attached via methods
	private Button addBtn, clearBtn, removeBtn, submitBtn;

	public ButtonPane() {

		this.setAlignment(Pos.CENTER);
		this.setSpacing(15);

		addBtn = new Button("Add");
		clearBtn = new Button("Clear");
		removeBtn = new Button("Remove"); 
		submitBtn = new Button("Submit");

		this.getChildren().addAll(addBtn, clearBtn, removeBtn, submitBtn);

		for (Node n : this.getChildren()) {
			((Button) n).setPrefSize(70, 30); //set preferred size for each button
		}

	}

	//these methods allow listeners to be externally attached to this view

	/* ---TODO - implement addAddHandler method ---*/


	/* ---TODO - implement addClearHandler method ---*/


	/* ---TODO - implement addRemoveHandler ---*/


	/* ---TODO - implement addSubmitHandler ---*/

	
}
