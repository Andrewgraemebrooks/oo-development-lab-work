package controller;


import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

import model.Name;
import model.Register;

import view.ButtonPane;
import view.NamePane;
import view.RegisterRootPane;
import view.ViewPane;
import view.RegisterMenuBar;


public class RegisterController {

	//fields to be used throughout the class
	private NamePane np;
	private ButtonPane bp;
	private ViewPane vp;
	private RegisterMenuBar rmb;
	private Register model;

	public RegisterController(RegisterRootPane view, Register model) {
		/*--- TO DO - initialise view(s) and model fields ---*/

		//example of initialising the register menu bar for convenient access
		rmb = view.getMenuBar();

		//attach event handlers to view using private helper method
		this.attachEventHandlers();	
	}

	private void attachEventHandlers() {

		//bp.addSubmitHandler(new SubmitHandler()); //will work when you add appropriate method to the ButtonPane

		rmb.addExitHandler(e -> System.exit(0));
		rmb.addAboutHandler(e -> this.alertDialogBuilder(AlertType.INFORMATION, "Information Dialog", null, "Register MVC app v2.0"));

		/*---TO DO - attach handlers to view some may be lambdas and some named inner classes ---*/

	}

	//event handlers
	private class SubmitHandler implements EventHandler<ActionEvent> {

		public void handle(ActionEvent e) {
			//add contents of the ListView within the ViewPane to the model and then clear it
			vp.getContents().forEach(n -> model.add(n));
			vp.clearNames();
		}
	}

	/* ---TO DO - implement AddHandler ---*/

	/* ---TO DO - implement ClearHandler ---*/
	
	/* ---TO DO - implement RemoveHandler ---*/

	/* ---TO DO - implement IndividSelectHandler ---*/

	/* ---TO DO - implement MultiSelectHandler ---*/

	/* ---TO DO - implement SaveMenuHandler ---*/

	/* ---TO DO - implement LoadMenuHandler ---*/
	

	//helper method to build dialogs - you may wish to use this during certain event handlers
	private void alertDialogBuilder(AlertType type, String title, String header, String content) {
		Alert alert = new Alert(type);
		alert.setTitle(title);
		alert.setHeaderText(header);
		alert.setContentText(content);
		alert.showAndWait();
	}

}
