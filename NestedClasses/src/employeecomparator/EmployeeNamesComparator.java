package employeecomparator;

import java.util.Comparator;

public class EmployeeNamesComparator implements Comparator<Employee> {
	
	@Override
	public int compare(Employee e1, Employee e2) {
		return Integer.compare(e2.getName().getFullName().length(),
				e2.getName().getFullName().length());
	}

}
