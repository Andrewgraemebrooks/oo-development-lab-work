package textio;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class NameProcessor {
	public static void main(String[] args) throws FileNotFoundException {
		ArrayList<Name> arrayList = new ArrayList<>();
		Scanner in = new Scanner(new File("names.txt"));
		
		String line;
		String[] arr;
		
		while (in.hasNextLine()) {
			line = in.nextLine();
			
			arr = line.split(" ");
//			if (arr.length == 1) {
//				arrayList.add(new Name(arr[0], arr[1]));
//			}
			arrayList.add(new Name(arr[0], arr[1]));
//			for (String string : arr) {
//				System.out.println(string);
//			}
		}
		
		in.close();
		
		PrintWriter out = new PrintWriter(new File("shorter_names.txt"));
		
		for (Name name : arrayList) {
//			if (name.getFullName().length() <= 12) {
//				out.println(name.getFamilyName());
//			}
			out.println(name.getFullName());
		}
		
		
		
		out.close();
	}

}
