package binaryio;

public class PlaylistIOdemo {
	public static void main(String[] args) {
		PlayList playlist = new PlayList("90's Rap");
		playlist.addSong(new Song("All Eyez On Me", 302, "Tupac"));
		playlist.addSong(new Song("Juicy", 315, "Biggie"));
		
		System.out.println("Playlist 1 " + playlist.toString());
		
		System.out.println("Writing Playlist 1 to file...");
		playlist.writePlayListToFile();
		
		
		
	}
}
