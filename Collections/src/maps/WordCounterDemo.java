package maps;

public class WordCounterDemo {

	public static void main(String[] args) {
		WordCounter dictionary = new WordCounter();
		java.util.Scanner input = new java.util.Scanner(System.in);
		boolean flag = true;
		System.out.println("Type 'q' to quit!");
		
		while (flag) {
			System.out.println("Please enter the words:");
			dictionary.processWord(input.nextLine());
			if (input.nextLine().equals("q")) {
				flag = false;
			}
		}
		
		// Not sure how to print this out. Moving onto week 4.
	}

}
