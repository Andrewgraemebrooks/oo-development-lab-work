package maps;

import java.util.Map;
import java.util.TreeMap;

public class WordCounter {
	// Fields
	private Map<String, Counter> lookup;
	
	// Constructors
	public WordCounter() {
		lookup = new TreeMap<String, Counter>();
	}
	// Methods
	public void processWord(String word) {
		if (lookup.containsKey(word)) {
			lookup.get(word).increment();
		}
		else {
			lookup.put(word, new Counter(1));
		}
	}
	
	public int getWordCount(String word) {
		if (lookup.containsKey(word)) {
			return lookup.get(word).getCount();
		}
		else {
			return 0;
		}
	}

}
