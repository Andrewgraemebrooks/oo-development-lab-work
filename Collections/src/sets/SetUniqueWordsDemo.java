package sets;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class SetUniqueWordsDemo {
	public static void main(String[] args) {
//		Set<String> unique = new HashSet<>();
		Set<String> unique = new TreeSet<>();
		String sentence = "the cat in the hat";
		String[] words = sentence.split(" ");
		for (String word : words) {
			unique.add(word);
		}
		
		System.out.println("The HashSet size: " + unique.size());
		System.out.println(unique);
	}
}
