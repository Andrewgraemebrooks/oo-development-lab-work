package counter;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.paint.Color;

public class CounterPane extends BorderPane {
	// Fields
	int currentValue = 0;
	int buttonSize = 35;
	
	// Constructors
	public CounterPane() {
		FlowPane flowpane = new FlowPane();
		flowpane.setHgap(100);
		flowpane.setVgap(20);
		flowpane.setPrefWrapLength(100);
		flowpane.setPadding(new Insets(20));
		flowpane.setBackground(new Background(new BackgroundFill(Color.BISQUE, null, null)));
		
		// Add the flow pane to this border pane
		this.setCenter(flowpane);
		
		// Text Field
		TextField tf = new TextField();
		tf.setEditable(false);
		tf.setText(String.valueOf(Integer.toString(currentValue)));
		tf.setPrefWidth(175);
		tf.setAlignment(Pos.CENTER);
		flowpane.getChildren().add(tf);
		
		// Decrement Button
		Button decrementButton = new Button("-");
		decrementButton.setPrefSize(buttonSize, buttonSize);
		decrementButton.setAlignment(Pos.CENTER_LEFT);
		flowpane.getChildren().add(decrementButton);
		decrementButton.setOnAction(e -> tf.setText(Integer.toString(currentValue--)));
//		decrementButton.setOnAction(e -> currentValue--);
		
		// Increment Button
		Button incrementButton = new Button("+");
		incrementButton.setPrefSize(buttonSize, buttonSize);
		incrementButton.setAlignment(Pos.CENTER_RIGHT);
		flowpane.getChildren().add(incrementButton);
		incrementButton.setOnAction(e -> tf.setText(Integer.toString(currentValue++)));
//		incrementButton.setOnAction(e -> currentValue++);
		
		// Issue is that when the other button is pressed it performs the action of the other button once before its own.
		
	}
}
