package songcomparator;

import java.util.Comparator;

public class PlayListApp {

    public static void main(String[] arg) {

    	//Create a new PlayList object
        PlayList playlist = new PlayList("My Favourites");

        //add some new Song objects
        playlist.addSong( new Song("Only Girl (In The World)", 235, "Rhianna") );
        playlist.addSong( new Song("Thinking of Me", 206, "Olly Murs") );
        playlist.addSong( new Song("Raise Your Glass", 202,"P!nk") );
        playlist.addSong( new Song("Beat it", 150,"M Jackson") );        
        playlist.addSong( new Song("Bad", 210,"M Jackson") );
        
      
        System.out.println(playlist.getTrackListings());   
        
        
        // ADD CODE HERE...
        // Reverse Order
        System.out.println("Reverse Order");
        playlist.sortPlaylist(Comparator.reverseOrder());
        System.out.println(playlist.getTrackListings());   
        
        // Lambda Expression 1
        System.out.println("Lambda Expression Comparing Songs through their artists");
        playlist.sortPlaylist((Song s1, Song s2) -> s1.getArtist().compareTo(s2.getArtist()));
        System.out.println(playlist.getTrackListings());
        
        // Lambda Expressions 2
        System.out.println("Lambda Expression Comparing Songs through various means");
        playlist.sortPlaylist(Comparator
        		.comparing((Song x) -> x.getArtist())
        		.thenComparing((Song x) -> x.getDuration())
        		.thenComparing((Song x) -> x.getTitle()));
        System.out.println(playlist.getTrackListings());
        
        // Method References 1
        System.out.println("Lambda Expression using method references aka \"::\"");
        playlist.sortPlaylist(Comparator
        		.comparing(Song::getArtist)
        		.thenComparing(Song::getDuration)
        		.thenComparing(Song::getTitle));
        System.out.println(playlist.getTrackListings());
        
        // Test of a method reference for printing every song on the Play list.
        playlist.forEach(System.out::println);
        
        // Method References 2
        System.out.println("\nLambda Expression using method references to reverse order\n"
        		+ "This would produce the effect of artist(desc), duration(desc), then title(desc)");
        playlist.sortPlaylist(Comparator
        		.comparing(Song::getArtist)
        		.thenComparing(Song::getDuration)
        		.thenComparing(Song::getTitle).reversed());
        System.out.println(playlist.getTrackListings());
        
        // Method References 3
        // As you may remember you can overload functions, e.g. run() and run(human).
        // Use are simply using a different thenComparing method here (an overloaded one).
        System.out.println("\nMethod references to reverse order of duration only\n"
        		+ "This would produce the effect of artist(asc), duration(desc), then title(asc)");
        playlist.sortPlaylist(Comparator
        		.comparing(Song::getArtist)
        		.thenComparing(Song::getDuration, Comparator.reverseOrder()) // Use the overloaded thenComparing method.
        		.thenComparing(Song::getTitle));
        System.out.println(playlist.getTrackListings());
               
    }
}
