package namecomparator;

import java.util.Comparator;

public class RegisterDemo {
	public static void main(String[] args) {
		Register register = new Register();
		Name andrew = new Name("Andrew", "Brooks");
		Name brian = new Name("Brian", "May");
		Name chris = new Name("Chris", "Brooks");
		register.addName(andrew);
		register.addName(brian);
		register.addName(chris);
		
		System.out.println("Sorting the register in reverse order");
		register.sortRegister(Comparator.reverseOrder());
		register.forEach(System.out::println);
		
		System.out.println("\nSorting the register by first name w/ Lambda Expression");
		register.sortRegister(Comparator.comparing((Name x) -> x.getFirstName()));
		register.forEach(System.out::println);
		
		System.out.println("\nSorting the register by family name w/ Method Reference");
		register.sortRegister(Comparator.comparing(Name::getFamilyName));
		register.forEach(System.out::println);
	}
}
