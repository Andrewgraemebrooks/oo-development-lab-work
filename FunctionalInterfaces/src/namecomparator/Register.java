package namecomparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

/**
 * A register has an Arraylist of names.
 * @author Andrew Brooks - P16160343
 *
 */
public class Register implements Iterable<Name> {
	// Fields
	private ArrayList<Name> register;

	// Constructors
	
	/** Default constructor, initialises the register as a new empty Arraylist.
	 * 
	 */
	public Register() {
		register = new ArrayList<>();
	}
	
	// Methods
	
	/** Adds a name to the register 
	 * 
	 * @param name The name to add to the register.
	 */
	public void addName(Name name) {
		register.add(name);
	}
	
	/** Removes a name from the register
	 * 
	 * @param name The name to remove from the register
	 */
	public void removeName(Name name) {
		register.remove(name);
	}
	
	/** Removes and returns a name at a specific index.
	 * 
	 * @param index The specific index of the name to be removed.
	 * @return The specific removed name to be returned.
	 */
	public Name removeName(int index) {
		return register.remove(index);
	}
	
	/** Returns a name at a specific index.
	 * 
	 * @param index The specific index of the name to be returned.
	 * @return The specific name to be returned.
	 */
	public Name getName(int index) {
		return register.get(index);
	}
	
	/** Returns the size of the register.
	 * 
	 * @return The size of the register.
	 */
	public int registerSize() {
		return register.size();
	}
	
	/** Clears the register (removes every name on the register).
	 * 
	 */
	public void clearRegister() {
		register.clear();
	}
	
	/** Checks whether the register is empty.
	 * 
	 * @return Whether or not the register is empty.
	 */
	public boolean isRegisterEmpty() {
		return register.isEmpty();
	}
	
	/** Searches the entire register to check whether a name with a specific family name
	 * is present. If the family name is present the method will return true, or else it
	 * will return false.
	 * 
	 * @param surname The specific family name to search the register for.
	 * @return Whether or not the family name is present in the register.
	 */
	public boolean searchRegisterByFamilyName (String surname) {
		for (int i = 0; i < register.size(); i++) {
			Name n = register.get(i);
			if (n.getFamilyName().equals(surname)) return true;
		}
		
		return false;
	}
	
	/** Counts the number of first names in the register that end with a specific
	 * character. Returns the number of occurrences.
	 * 
	 * @param character The specific character to search the register with.
	 * @return The number of occurrences.
	 */
	public int countFirstNameOccurrences(char character) {
		int count = 0;
		for (int i = 0; i < register.size(); i++) {
			Name n = register.get(i);
			if (n.getFirstName().charAt(n.getFirstName().length()-1) == character) count++;
		}
		return count;
	}
	
	/** Returns the register's iterator.
	 * 
	 */
	public Iterator<Name> iterator() {
		return register.iterator();
	}
	
	/** Sorts the register. 
	 * 
	 */
	public void sortRegister() {
		Collections.sort(register);
	}
	
	/** Sorts the register to a custom comparator
	 * 
	 * @param comparator The custom comparator
	 */
	public void sortRegister(Comparator<Name> comparator) {
		register.sort(comparator);
	}
	
	/** Returns a textual representation of the Register.
	 * @return A textual representation of the register.
	 */
	@Override
	public String toString() {
		return "Register:[" + register + "]";
	}
}
