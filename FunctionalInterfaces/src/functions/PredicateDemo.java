package functions;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;

public class PredicateDemo {

	public static void main(String[] args) {
		List<String> friends = Arrays.asList("Brian", "Nate", "Neal", "Raju", "Sara", "Scott");
		
		long noStartWithN = friends.stream().filter(name -> name.startsWith("N")).count();
		System.out.println(noStartWithN+"\n");
		
		Predicate<String> startsWithN = name -> name.startsWith("N");
		Predicate<String> startsWithB = name -> name.startsWith("B");
		
		noStartWithN = friends.stream().filter(startsWithN).count();
		long noStartWithB = friends.stream().filter(startsWithB).count();
		
		// Custom Function that accepts a string and returns a predicate.
		Function<String, Predicate<String>> startsWithLetter =
				(String letter) -> {
					Predicate<String> checkStarts = (String name) ->
				name.startsWith(letter);
					return checkStarts;
				};
				
		// Custom Function that accepts a string and returns a predicate using lambdas.
		
		Function<String, Predicate<String>> startsWithLetterr = 
				(String letter) -> ((String name) -> name.startsWith(letter));
		
		// Removing the types and letting the compile infer the types
		
		Function<String, Predicate<String>> startsWithLetterrr =
				letter -> name -> name.startsWith(letter);
	}

}
