package register;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Control;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Slider;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.util.StringConverter;

/** A scene graph of examples of FX UI controls */
public class RegisterRootPane extends FlowPane{

	public RegisterRootPane() {
		/*
		 * This pane is a FlowPane with left to right layout (by default),
		 * and horizontal and vertical gaps between its children (set as
		 * parameters)
		 */
		//set this FlowPane's properties
		this.setHgap(20);
		this.setVgap(20);
		this.setPrefWrapLength(600);
		this.setPadding(new Insets(20)); // create a 20px margin between
											// flowpane and the window
		this.setBackground(new Background(new BackgroundFill(Color.STEELBLUE, null, null)));

		
		/*=== Create and add example UI control children to this pane======*/
		
		// Label
		Label lb = new Label("Title");
		lb.setFont(Font.font("Arial", 14));
		lb.setTextFill(Color.BLACK);
		this.getChildren().add(lb);
	}
}
